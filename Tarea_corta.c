#include <stdio.h>
#include <math.h>

//Autor: Gerson Loaiza V�squez
//Carne: 2020207712

//Entradas: Un n�mero entero
//Salidas: Un valor booleano que determina si el n�mero es par o no
//Objetivo: Determinar si un n�mero ingresado es par o impar
void es_par(int numero){
    if (numero % 2 == 0){
        printf("Si, es par");
    }
    else{
        printf("No es par");
    }
}


//Entradas: Un n�mero entero
//Salidas: El fibonacci de un n�mero ingresado
//Objetivo: Determinar la sucesi�n fibonacci de un n�mero
int fibonacci(int numero){
    if(numero == 1 || numero == 0){
        return numero;
    }
    else{
        return fibonacci(numero - 1) + fibonacci(numero - 2);
    }
}


//Entradas: Un n�mero entero
//Salidas: El factorial de un n�mero ingresado
//Objetivo: Determinar el factorial de un n�mero
int factorial(int numero){
    if (numero == 1){
        return numero;
    }
    else{
        return (factorial(numero - 1) * numero);
    }
}


//Entradas: Un n�mero entero
//Salidas: Un n�mero que ser� la cantidad de d�gitos del n�mero ingresado
//Objetivo: Calcula la cantidad de d�gitos de un n�mero ingresado con iteraci�n while
long contar_digitos(long numero){
    long contador = 0;

    if (numero == 0){
        return 1;
    }
    while (numero != 0){
        contador++;
        numero = numero / 10;
    }
    return contador;
}


//Entradas: Un n�mero entero
//Salidas: Un n�mero que ser� la cantidad de d�gitos del n�mero ingresado
//Objetivo: Cuenta los d�gitos de un n�mero ingresado con iteraci�n for
long contar_digitos_for(long numero){
    long contador;
    if (numero == 0){
        return 1;
    }
    for (contador = 0; numero != 0; contador++){
        numero = numero / 10;
    }
    return contador;
}


//Entradas: Un n�mero entero
//Salidas: La sumatoria hasta un n�mero determinado
//Objetivo: Calcula la sumatoria de un n�mero ingresado con iteraci�n for
int sumatoria_for(int numero){
    int contador, resultado;
    for (contador = 0; contador <= numero; contador++){
        resultado += contador;
    }
    return resultado;
}


//Entradas: Un n�mero entero
//Salidas: La sumatoria hasta un n�mero determinado
//Objetivo: Calcula la sumatoria de un n�mero ingresado con iteraci�n while
int sumatoria_while(int numero){
    int contador, resultado = 0;
    while (contador <= numero){
        resultado += contador;
        contador++;
    }
    return resultado;
}


//Entradas: Un n�mero entero
//Salidas: El n�mero ingresado al rev�s
//Objetivo: Crea un nuevo n�mero a partir del n�mero ingresado con iteraci�n while
int invertir_numero(int numero){
    int nuevo_numero = 0;
    while (numero != 0){
        nuevo_numero = (nuevo_numero * 10) + (numero % 10);
        numero = numero / 10;
    }
    return nuevo_numero;
}


//Entradas: Un n�mero entero
//Salidas: True si el n�mero es pal�ndromo, false de lo contrario
//Objetivo: Determinar si un n�mero ingresado es pal�ndromo o no
void palindromo(int num){
    int largo_numero = contar_digitos_for(num);

    int largo_numero_mitad_par = largo_numero / 2;
    int largo_numero_mitad_impar = largo_numero / 2 + 1;

    int exponente_izquierda_par = pow(10, largo_numero_mitad_par);
    int exponente_izquierda_impar = pow(10, largo_numero_mitad_impar);
    int exponente = pow(10, largo_numero / 2);

    int mitad_numero_derecho = num % exponente;
    int mitad_numero_derecho_invertido = invertir_numero(mitad_numero_derecho);

    int numero_parte_izquierda;

    if (largo_numero % 2 == 0){
        numero_parte_izquierda = num / exponente_izquierda_par;
    }
    else{
        numero_parte_izquierda = num / exponente_izquierda_impar;
    }
    if (numero_parte_izquierda == mitad_numero_derecho_invertido){
        printf("Es palindromo");
    }
    else{
        printf("No es palindromo");
    }
}


int main(){
    //es_par(3);
    //printf("%d", fibonacci());
    //printf("%d", factorial());
    //printf("%d", contar_digitos());
    //printf("%d", contar_digitos_for());
    //printf("%d", sumatoria_for());
    //printf("%d", sumatoria_while());
    //printf("%d", invertir_numero());
    //palindromo(0);
    printf("\n\nFin del programa\n");
    return 0;
}

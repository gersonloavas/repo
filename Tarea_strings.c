#include <stdio.h>
#include <stdlib.h>
//Autor: Gerson Loaiza V�squez
//Carn�: 2020207712

//Entradas: Un texto
//Salidas: Un n�mero
//Objetivo: Cuenta la cantidad de caracteres en una cadena de texto
int largo_texto(char* texto){
    int contador = 0;
    while(texto[contador] != '\0'){
        contador++;
    }
    return contador;
}


//Entradas: Un texto y un caracter objetivo
//Salidas: Un nuevo texto
//Objetivo: Busca el caracter objetivo en el texto de entrada y lo elimina
char* eliminar_caracter(char* texto_entrada, char caracter_objetivo){
    int contador, i = 0;
    char* texto_nuevo = calloc(largo_texto(texto_entrada) + 1, sizeof(char));
    while(texto_entrada[contador] != '\0'){
        if (texto_entrada[contador] == caracter_objetivo){
            contador++;
        }
        else{
            texto_nuevo[i] = texto_entrada[contador];
            contador++;
            i++;
        }
    }
    return texto_nuevo;
}


//Entradas: Un texto
//Salidas: Un nuevo texto
//Objetivo: Invierte por completo un texto de entrada
char* invertir_texto(char* texto){
    int contador = largo_texto(texto) - 1;
    int i = 0;
    char* texto_invertido = calloc(largo_texto(texto) + 1, sizeof(char));
    for (contador; contador >= 0; contador--){
        texto_invertido[i] = texto[contador];
        i++;
    }
    return texto_invertido;
}


//Entradas: Un texto
//Salidas: Un nuevo texto
//Objetivo: Almacena un literal en memoria heap
char* literal_to_heap(char* texto){
    int contador = 0;
    char* texto_heap = calloc(largo_texto(texto) + 1, sizeof(char));
    while(texto[contador] != '\0'){
        texto_heap[contador] = texto[contador];
        contador++;
    }
    return texto_heap;
}



int main(){
    //printf("|%s|", largo_texto())
    //printf("|%s|", eliminar_caracter("Hola mundocccccc", 'c'));
    //printf("|%s|", invertir_texto("hola"));
    //printf("|%s|", literal_to_heap("Literal to heap"));
    printf("\n\nFin del programa\n");
    return 0;
}
